package monitoring;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.*;

import static org.junit.Assert.assertEquals;

public class MonitoringServiceTests {
    private final InputStream systemIn = System.in;
    private final PrintStream systemOut = System.out;

    private ByteArrayInputStream testIn;
    private ByteArrayOutputStream testOut;

    @Before
    public void setUpOutput() {
        testOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOut));
    }

    private void stdoutInput(String data) {
        testIn = new ByteArrayInputStream(data.getBytes());
        System.setIn(testIn);
    }

    private String getContent() {
        return testOut.toString();
    }

    @After
    public void restore() {
        System.setIn(systemIn);
        System.setOut(systemOut);
    }
    @Test
    public void testAdd() {
        String str = "Junit is working fine";
        assertEquals("Junit is working fine",str);
    }
    @Test
    public void computeBasicStatsSmallInput(){
        MonitoringService service= MonitoringService.builder().build();
        List<String> lines = Arrays.asList(
                "10.0.0.2,-,apache,1549573860,GET /api/user HTTP/1.0,200,1234",
                "10.0.0.4,-,apache,1549573860,GET /api/user HTTP/1.0,200,1234",
                "10.0.0.4,-,apache,1549573860,GET /api/user HTTP/1.0,200,1234",
                "10.0.0.2,-,apache,1549573860,GET /api/help HTTP/1.0,200,1234",
                "10.0.0.5,-,apache,1549573860,GET /api/help HTTP/1.0,200,1234",
                "10.0.0.4,-,apache,1549573859,GET /api/help HTTP/1.0,200,1234"
        );

        List<HttpReqLog> logs= new ArrayList<>();
        for(String l:lines){
            logs.add(service.getLog(l));
        }
        final String testString = "- Most hit sections: [/api]\n" +
                "[host, request_count]: {10.0.0.4=3, 10.0.0.5=1, 10.0.0.2=2}\n";
        stdoutInput(testString);

        service.computeBasicStats(logs);
        assertEquals(testString,getContent());

    }



    @Test
    public void getMostHitSectionTest(){
        MonitoringService service= MonitoringService.builder().build();
        Map<String, Integer> m=new HashMap<>(){{
            put("/api",2);
            put("/report",1);
            put("/view",10);
        }};
        List<String> hits=service.getMostHitSections(m);
        List<String> expected=Arrays.asList("/view");
        assertEquals(expected,hits);

    }

    @Test
    public void showTenSecondStatsTest(){
        MonitoringService service= MonitoringService.builder().build();
        List<HttpReqLog> requests= new ArrayList<>();
        long startTime=1549573860;
        HttpReqLog log=service.getLog("10.0.0.2,-,apache,1549573862,GET /api/user HTTP/1.0,200,1234");
        HttpReqLog log2=service.getLog("10.0.0.2,-,apache,1549573872,GET /api/user HTTP/1.0,200,1234");
        HttpReqLog log3=service.getLog("10.0.0.2,-,apache,1549573858,GET /api/user HTTP/1.0,200,1234");
        service.showTenSecondStats(startTime,log,requests);
        long actualEventTime=requests.get(0).eventTime;

        assertEquals(1,requests.size());
        assertEquals(1549573862, actualEventTime);

        service.showTenSecondStats(startTime,log2,requests);
        service.showTenSecondStats(startTime,log3,requests);

        actualEventTime=requests.get(0).eventTime;
        assertEquals(1,requests.size());
        assertEquals(1549573872, actualEventTime);

    }

    @Test
    public void alertWatcherValidateRequests(){
        MonitoringService service= MonitoringService.builder()
                .alertLimit(10)
                .trafficCount(2)
                .currTime(1549573862)
                .alertTriggered(false)
                .requests(new TreeMap<>())
                .build();

        HttpReqLog log=service.getLog("10.0.0.2,-,apache,1549573859,GET /api/user HTTP/1.0,200,1234");
        HttpReqLog log2=service.getLog("10.0.0.2,-,apache,1549573000,GET /api/user HTTP/1.0,200,1234");

        service.alertWatcher(120,log);
        service.alertWatcher(120,log2);

        Map<Long, Integer> req=service.requests;
        Map<Long, Integer> expected= new TreeMap<>();
        expected.put(1549573859L,1);
        assertEquals(expected,req);
        assertEquals(1,service.trafficCount);

    }


    @Test
    public void alertWatcherValidateTrigger(){
        MonitoringService service= MonitoringService.builder()
                .alertLimit(2)
                .trafficCount(360)
                .currTime(1549573862)
                .alertTriggered(false)
                .requests(new TreeMap<>())
                .build();

        HttpReqLog log=service.getLog("10.0.0.2,-,apache,1549573859,GET /api/user HTTP/1.0,200,1234");

        service.alertWatcher(120,log);

        assertEquals(true,service.alertTriggered);


    }

    @Test
    public void alertWatcherValidateRecover(){
        MonitoringService service= MonitoringService.builder()
                .alertLimit(2)
                .trafficCount(360)
                .currTime(1549573862)
                .alertTriggered(false)
                .requests(new TreeMap<>())
                .build();

        HttpReqLog log=service.getLog("10.0.0.2,-,apache,1549573859,GET /api/user HTTP/1.0,200,1234");
        service.alertWatcher(120,log);
        service.setTrafficCount(239);
        service.alertWatcher(120,log);
        assertEquals(false,service.alertTriggered);
    }
}


