package ververica;

import com.google.gson.Gson;
import monitoring.HttpReqLog;
import monitoring.MonitoringService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.*;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.*;

import static org.junit.Assert.*;

public class MiniKafkaTest {
    private final InputStream systemIn = System.in;
    private final PrintStream systemOut = System.out;

    private ByteArrayInputStream testIn;
    private ByteArrayOutputStream testOut;

    @Before
    public void setUpOutput() {
        testOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOut));
    }

    private void stdoutInput(String data) {
        testIn = new ByteArrayInputStream(data.getBytes());
        System.setIn(testIn);
    }

    private String getContent() {
        return testOut.toString();
    }

    @After
    public void restore() {
        System.setIn(systemIn);
        System.setOut(systemOut);
    }

    @Test
    public void testAdd() {
        String str = "Junit is working fine";
        assertEquals("Junit is working fine", str);
    }

    @Test
    public void testStateless() {
        MiniKafka miniKafka = MiniKafka.builder()

                .queue(new LinkedList<>())
                .build();

        assertEquals(Collections.singletonList("ERR"), miniKafka.get(1));

        miniKafka.put("the");
        miniKafka.put("quick brown");
        miniKafka.put("fox jumps over the");
        miniKafka.put("lazy dog");
        assertEquals(new LinkedList<>(Arrays.asList("the", "quick brown")), miniKafka.get(2));

        miniKafka.put("the");
        miniKafka.put("quick brown");

        assertEquals(Collections.singletonList("ERR"), miniKafka.get(42));

        assertEquals(Collections.singletonList("fox jumps over the"), miniKafka.get(1));


    }

    @Test
    public void testStateful() {
        MiniKafka miniKafkaStateful = MiniKafka.builder()

                .queue(new LinkedList<>())
//                .gson(new Gson())
                .filePath("/tmp/lineQueue.json")
                .build();


        miniKafkaStateful.put("the");
        miniKafkaStateful.put("quick brown");
        miniKafkaStateful.put("fox jumps over the");
        miniKafkaStateful.put("lazy dog");
        assertEquals(new LinkedList<>(Arrays.asList("the", "quick brown")), miniKafkaStateful.get(2));

        miniKafkaStateful.shutdown();
        miniKafkaStateful.restoreState();

        assertEquals(Collections.singletonList("fox jumps over the"), miniKafkaStateful.get(1));

    }


}
