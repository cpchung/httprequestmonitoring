//package monitoring
//
//import java.util
//import java.util.TreeMap
//import java.util.logging.{Level, Logger}
//
//object HelloWorld {
//
//
//  private val LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME)
//  def main(args: Array[String]): Unit = {
//    println("Hello, world!")
//
//
////    args = Array[String]("/home/tom/httprequestmonitoring/src/main/resources/sample_small.txt")
////    args = Array[String]("/home/tom/httprequestmonitoring/src/main/resources/sample_csv.txt")
//
//    if (args.length < 1) {
//      LOGGER.log(Level.INFO, "Missing required parameter.")
//      return
//    }
//    val monitoringService: MonitoringService = MonitoringService
//      .builder
//      .streamSource(args(0))
//      .alertLimit(10)
//      .currTime(MonitoringService.SYS_TIME)
//      .trafficCount(0)
//      .alertTriggered(false)
//      .alertWindow(120)
//      .interval(10)
//      .requests(new util.TreeMap[Long, java.lang.Long,Integer])
//      .build
//
//    if (args.length == 2) monitoringService.setAlertLimit(args(1).toInt)
//
//    monitoringService.startMonitor()
//  }
//}