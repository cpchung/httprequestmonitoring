package ververica;


import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Data;

@Generated("jsonschema2pojo")

@Data
@Builder
public class Line {

    @SerializedName("line")
    @Expose
    public String line = null;

}