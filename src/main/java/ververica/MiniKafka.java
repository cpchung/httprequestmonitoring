package ververica;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.stream.JsonReader;
import lombok.Builder;
import lombok.Data;

import java.io.*;
import java.util.*;

@Data
@Builder
public class MiniKafka {

    public static void main(String[] args) {

        MiniKafka miniKafkaStateful = MiniKafka.builder()

                .queue(new LinkedList<>())
//                .gson(new Gson())
                .filePath("/tmp/lineQueue.json")
//                .filePath("/home/tom/httprequestmonitoring/src/main/java/ververica/lines.json")
                .build();


        miniKafkaStateful.put("the");
        miniKafkaStateful.put("quick brown");
        miniKafkaStateful.put("fox jumps over the");
        miniKafkaStateful.put("lazy dog");
        System.out.println(miniKafkaStateful.get(2));

        miniKafkaStateful.shutdown();

        miniKafkaStateful.restoreState();

        System.out.println(miniKafkaStateful.get(1));


    }


    //LinkedList will not have sequential read
//    ArrayList , then we need to use and maintain offset

    String filePath;
    List<String> queue;


//
//    PUT text
//    Inserts a text line into the FIFO Queue. text is a single line of an arbitrary
//    number of case sensitive alphanumeric words ([A-Z]+[a-z]+[0-9]) separated by
//    space characters.

    public void put(String text) {
        queue.add(text);


    }

    //    GET n
//    Return n lines from the head of the queue and remove them from the queue.
//    If n is not a valid line count, the server should return a "ERR\r\n" without
//    dequeuing any elements.
    public List<String> get(int n) {

        if (n > queue.size()) {
            System.out.println("ERR");
            return new LinkedList<>(Arrays.asList("ERR"));
        }

        List<String> res = new LinkedList<>();
        for (int i = 0; i < n; i++) {
            String out = queue.remove(0);
            System.out.println(out);
            res.add(out);
        }

        return res;

    }

    //    SHUTDOWN
//    Shutdown the application/server.
//In this case, if the application is restarted from the same current working
//directory, it should restore the state of the queue as it was before the shutdown.
    public void shutdown() {
        //todo: can do better to handle power/network outage
        checkPoint();

    }

    void checkPoint() {

        int n = queue.size();
        Line[] lines = new Line[n];


        Iterator<String> itr = queue.iterator();

        int i = 0;
        while (itr.hasNext()) {
            lines[i] = new Line(itr.next());
            i++;
        }

        try (FileWriter writer = new FileWriter(filePath)) {
            Gson gson = new Gson();
//            FileWriter writer = new FileWriter(filePath); // does not work
            gson.toJson(lines, writer);

        } catch (IOException e) {
            e.printStackTrace();
        }

        queue.clear();
    }

    public void restoreState() {
        //todo: can do better to handle power/network outage
        try (Reader reader = new FileReader(filePath)) {

            Gson gson = new Gson();
            // Convert JSON to JsonElement, and later to String
            JsonElement json = gson.fromJson(reader, JsonElement.class);

            String jsonInString = gson.toJson(json);
            System.out.println(jsonInString);

            Line[] lines = new Gson().fromJson(jsonInString, Line[].class);

            for (Line line : lines) {
                queue.add(line.getLine());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    //    The server should listen for connections on TCP port 10042.
    void quit() {
    }

}
