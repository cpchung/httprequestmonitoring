package monitoring;

public enum RequestStatus {
    OK(200);
    public final int statusCode;
    RequestStatus(int statusCode){
        this.statusCode=statusCode;
    }
}
