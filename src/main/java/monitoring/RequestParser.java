package monitoring;

import java.util.Arrays;
import java.util.List;

class RequestParser {

    private RequestParser(){}

    public static List<String> parse(String stream) {
        return Arrays.asList(stream.split(","));
    }

    public static String getPathSection(String request) {
        String path = request.split("\\s+")[1];
        int index = path.indexOf("/", 1);
        if (index == -1) return path;
        return path.substring(0, index);
    }
}
