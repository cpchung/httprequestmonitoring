package monitoring;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class HttpReqLog {

    Long eventTime;

    String remoteHost;

    Integer statusCode;

    String rfc931;

    String authuser;

    String request;

    String bytes;


}

