package monitoring;

import lombok.Builder;
import lombok.Data;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@Builder
@Data
public class MonitoringService {

    private static final  Logger LOGGER =
            Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    //for simplicity, using first date in the test data as the current time of the system.
    static final int SYS_TIME = 1549573860;
    long currTime;
    int interval;
    int alertLimit;
    int alertWindow;
    int trafficCount;
    boolean alertTriggered;

    String streamSource;
    Map<Long, Integer> requests;


    public void startMonitor() {


        try (Scanner scanner = new Scanner(new File(streamSource))) {

            if (scanner.hasNextLine()) scanner.nextLine(); //skip parsing file header
            List<HttpReqLog> requestsEveryTenSecs = new ArrayList<>();
            long start= SYS_TIME;
            while (scanner.hasNextLine()) {

                HttpReqLog log = getLog(scanner.nextLine());

                this.trafficCount++;
//                10s metrics
                showTenSecondStats(start, log, requestsEveryTenSecs);
//                alerts
                alertWatcher(alertWindow, log);
//                update interval time
                if(log.getEventTime()-start>=this.interval){
                    start= log.getEventTime();
                }
//                update system time
                if (log.getEventTime() > this.currTime) {
                    this.currTime = log.getEventTime();
                }
            }
            if(!requestsEveryTenSecs.isEmpty()) computeBasicStats(requestsEveryTenSecs);

        } catch (IOException exception) {
            LOGGER.log(Level.SEVERE, exception.getMessage());
        }

    }

    // metrics: most hit sections, request count for remote hosts, failed requests
    public void computeBasicStats(List<HttpReqLog> logs) {
        Map<String, Integer> counts = new HashMap<>();
        Map<String, Integer> hosts = new HashMap<>();
        Map<Integer, Set<String>> badReqs = new HashMap<>();
        for (HttpReqLog log : logs) {
            String section = RequestParser.getPathSection(log.getRequest());
            String remoteHost = log.getRemoteHost();
            int reqStatus = log.getStatusCode();

            counts.put(section, counts.getOrDefault(section, 0) + 1);
            hosts.put(remoteHost, hosts.getOrDefault(remoteHost, 0) + 1);
            if (reqStatus != RequestStatus.OK.statusCode) {
                if (!badReqs.containsKey(reqStatus)) {
                    badReqs.put(reqStatus, new HashSet<>());
                }
                badReqs.get(reqStatus).add(remoteHost);
            }
        }
        printStats(counts, hosts, badReqs);
    }

    // traffic count in past 2 minutes
    public void alertWatcher(int window, HttpReqLog log) {
        long eventTime=log.getEventTime();
        this.requests.put(eventTime, this.requests.getOrDefault(eventTime, 0) + 1);

        this.requests.forEach((t, count) -> {
            if (t < this.currTime-window ) {
                this.trafficCount-=count;
            }
        });
        this.requests.keySet().removeIf(key -> key<this.currTime-window);

        double avg = (double)this.trafficCount / window;

        if (avg > this.alertLimit && !this.alertTriggered) {
            this.alertTriggered = true;

            LOGGER.log(Level.INFO, String.format("ALERT TRIGGERED: hits=%.2f, triggered at %s %n", avg, getTimeStamp(eventTime)));
        } else if (avg < this.alertLimit && this.alertTriggered) {
            this.alertTriggered = false;
            LOGGER.log(Level.INFO, String.format("ALERT RECOVERED: hits=%.2f, triggered at %s %n", avg, getTimeStamp(eventTime)));
        }
    }

    HttpReqLog getLog(String logString) {
        List<String> parts = RequestParser.parse(logString);

        return HttpReqLog.builder()
                .remoteHost(parts.get(0))
                .rfc931(parts.get(1))
                .authuser(parts.get(2))
                .eventTime(Long.parseLong(parts.get(3)))
                .request(parts.get(4))
                .statusCode(Integer.parseInt(parts.get(5)))
                .bytes((parts.get(6)))
                .build();
    }

    private void printStats(Map<String, Integer> counts, Map<String, Integer> hosts, Map<Integer, Set<String>> badReqs) {
        if (!counts.isEmpty()) {
           System.out.printf("- Most hit sections: %s%n", getMostHitSections(counts));
        }
        if (!hosts.isEmpty()) {
            System.out.printf("[host, request_count]: %s%n", hosts);
        }
        if (!badReqs.isEmpty()) {
            System.out.printf("failed requests: %s%n", badReqs);
        }
    }

//    Get the top hit section
   List<String> getMostHitSections(Map<String, Integer> m) {

        Map<Integer, List<String>> top = new TreeMap<>(Collections.reverseOrder());
        for (Map.Entry<String, Integer> en : m.entrySet()) {
            if (!top.containsKey(en.getValue())) {
                top.put(en.getValue(), new ArrayList<>());
            }

            top.get(en.getValue()).add(en.getKey());
        }
        Map.Entry<Integer, List<String>> entry = top.entrySet().iterator().next();
        return entry.getValue();

    }

//    format unix time to readable datetime
    private String getTimeStamp(long epochTime) {
        Date date = new Date(epochTime * 1000L);
        SimpleDateFormat formatDate = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss z");
        formatDate.setTimeZone(TimeZone.getTimeZone("UTC"));
        return formatDate.format(date);
    }

//    logic for handling 10s interval metrics
    void showTenSecondStats(long startTime, HttpReqLog log, List<HttpReqLog> requests) {
        long eventTime = log.getEventTime();
        if (eventTime >= startTime && eventTime - startTime < interval) {

            requests.add(log);
        } else if (eventTime - startTime >= interval) {
//          get metrics for every 10s lines of the log file
            computeBasicStats(requests);
            // clear the list before getting requests for the next 10s.
            requests.clear();
            requests.add(log);
        }
    }
}
