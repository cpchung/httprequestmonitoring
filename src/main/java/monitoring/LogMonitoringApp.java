package monitoring;

import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/*Application Main */
public class LogMonitoringApp {
    private static final Logger LOGGER =
            Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public static void main(String[] args) {

        args = new String[]{"/home/tom/httprequestmonitoring/src/main/resources/sample_small.txt"};
        args = new String[]{"/home/tom/httprequestmonitoring/src/main/resources/sample_csv.txt"};

        if (args.length < 1) {
            LOGGER.log(Level.INFO, "Missing required parameter.");
            return;
        }
        MonitoringService monitoringService = MonitoringService
                .builder()
                .streamSource(args[0])
                .alertLimit(10)
                .currTime(MonitoringService.SYS_TIME)
                .trafficCount(0)
                .alertTriggered(false)
                .alertWindow(120)
                .interval(10)
                .requests(new TreeMap<>())
                .build();

        if (args.length == 2) {
            monitoringService.setAlertLimit(Integer.parseInt(args[1]));
        }

        monitoringService.startMonitor();


    }
}
