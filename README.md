# README #

HTTP request monitoring application

### Run ###
* compiled classes are located in the project directory /logmonitoring/target/classes
```bash
java -cp <project_dir>/logmonitoring/target/classes monitoring.LogMonitoringApp <inputFile path>
```

for example:
```bash
java -cp /home/tom/httprequestmonitoring/target/classes monitoring.LogMonitoringApp /home/tom/httprequestmonitoring/src/main/resources/sample_small.txt
```



* import to an IDE (Intellij) and run

### Approach ###
For this exercise, I'm assuming 1549573860 is the current system time since it's the first date from the log file. 
I'm also assuming that the logs arrived in real time, this means the date in the log line is 
the also current system time. So, the application updates the current time when getting a log line with date > current time.
The logs are not perfectly sorted by date because it is possible that some data arrived late.
Each log line is serialized to a HttpReqLog object. 
#### Metrics for every 10s of the log line ####
I've assumed every 10s log lines to be logs of a 10s window. Given the initial date 1549573860, 
the first 10s log lines are logs with date 1549573860 to 1549573869, and the second 10s interval
are the times from 1549573870 to 1549573879 and so on. Log lines with date earlier than the 
window are dropped from the calculation. 

When the difference between the log line date and current system time is larger than 10, start time 
of the 10s window is updated to the current system for collect data for the next 10s. 

* The metrics calculated are top section hits, host-count pairs, and failed requests. These provide
useful information for debugging problems.
* Add each request to a list until there are 10s worth of data or when reaching the end of the file. 
Pass this list to the helper function computeBasicStats(requests) for printing the metrics. The list is 
  emptied before storing the data for the next 10 seconds. 
* Use map data structure for storing count metrics. Use Treemap sorted in descending order to get most hit sections.
This will give O(1) access time for finding the most hit sections. 
#### Alerts ####
A 2-minute window is tracked using current system time and the log line date. Current system time
is updated when log line date is greater.
* Use a Treemap to group request counts by log line date
* If current time - log line date  <= 120 seconds, then the line is included in the Treemap for the average calculation. In this
  case those data arrived late could also be included in the calculation as long as it arrives within the 2 minutes. 
* Remove record with date less than the 2-minute window from the map and decrement total request count as needed. 
* Using a global flag to indicate when application has triggered an alert to prevent duplicate alerts. It is 
set to false when application has recovered. 


### Enhancement ###
Here are some of improvements that can be made on the application:
* Add concurrency to handle the logic 10s metrics and alerts
* For average number of requests calculation, handle the case when there
  could be no request for a period of time. 
* The helper methods can be part of a utility class
* More checks on the methods to handle bad inputs
* Add a configurations file for hardcoded values
* Add more tests and more complex test cases
* Add javadoc comments
* More logging 